package pizzashop.food;
/**
 * 
 * @author Prin Angkunanuwat
 *
 */
public interface OrderItem {
	/**
	 * Get string to describe orderitem.
	 * @return description of orderitem.
	 */
	String toString();
	/**
	 * Get price of orderitem.
	 * @return Price of orderitem.
	 */
	double getPrice();
	/**
	 * Set size of orderitem.
	 * @param size is a size to set to orderitem.
	 */
	void setSize(int size);
}

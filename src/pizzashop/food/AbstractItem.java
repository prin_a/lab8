package pizzashop.food;
/**
 * Abstract class implement Orderitem contain duplicate code in Pizza and Drink
 * @author Prin Angkunanuwat
 *
 */
public abstract class AbstractItem implements OrderItem {
	
	
	protected static final String [] sizes = { "None", "Small", "Medium", "Large" };
	protected int size;
	/** Initialize size of this item.
	 * 
	 * @param size of this item.
	 */
	public AbstractItem(int size) {
		super();
		this.size = size;
	}

	@Override
	/**.Set size of this item.
	 * 
	 */
	public void setSize(int size) {
		this.size = size;
	}

}

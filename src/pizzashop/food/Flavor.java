package pizzashop.food;
/** Collection of the drink favor.
 * 
 *
 */
public enum Flavor {
	COKE("Coke"),
	PEPSI("Pepsi"),
	SPRITE("Sprite"),
	ORANGE("Orange"),
	WATER("Water"),
	COFFEE("Coffee");
	
	Flavor(String name) { this.name = name; }
	/** @return description of the favor */
	public String toString() { return name; }
	private String name;
}
